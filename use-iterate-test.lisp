(asdf:load-system :iterate)
(asdf:load-system :agnostic-lizard)

#+sbcl (require :sb-rt)
#-sbcl (defpackage :regression-test (:export :deftest :rem-all-tests))
#-sbcl (defmacro regression-test::deftest ())
#-sbcl (defmacro regression-test::rem-all-tests ())

(loop 
  for expander in 
  (list
    (lambda (x) (agnostic-lizard:macroexpand-all x nil))
    (lambda (x) (agnostic-lizard:walk-form x nil))
    (lambda (x) (eval `(agnostic-lizard:macro-macroexpand-all ,x))))
  do
  (with-open-file (f "iterate-test.lisp")
    (eval (read f)) 
    (eval (read f))  
    (defmacro 
      #+sbcl sb-rt::deftest 
      #-sbcl regression-test::deftest
      (name expr &rest values) 
      `(if
         (ignore-errors 
           (equalp (multiple-value-list ,expr) ',values))
         (progn
           (format t "~s passed~%" ',name)
           t)
         (progn
           (format t "~s failed~%" ',name)
           nil)))
    (loop 
      for form := (ignore-errors (read f)) 
      for expanded := (funcall expander form) 
      while form 
      do (eval expanded))))
